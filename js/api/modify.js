export default function modifyContents(listElement) {

    // 1. 수정할 요소 선택
    let modifyedPk; // 수정할 요소 Pk
    for(let ele of listElement) { 
        ele.addEventListener("click", function() {
            for(let ele of listElement) ele.classList.remove("clicked");
            this.classList.add("clicked"); // 요소 선택 효과 적용
            modifyedPk = this.dataset.pk; 
        })
    }

    // 2. 수정 버튼 클릭시 모달창 띄우고 선택한 요소의 데이터 조회
    const modifyBtn = document.getElementById("modify"); 
    modifyBtn.addEventListener("click", (e) => { 
        console.log("clicked");
        let modalWrapper = document.querySelector(".modal-wrapper");
        modalWrapper.classList.remove("off");
        let category = e.target.dataset.type;

        if(category == "user" || category == "company") { // 회원정보 및 기업정보 조회
            axios.get("http://localhost:3000/user_info/" + modifyedPk)
                 .then((response) => { return response.data; })
                 .then((response) => { viewSelectInfoData(response, modifyedPk, category) })
                 .catch((error) => console.log(error))
        } else if(category == "webtoon") { // 웹툰 정보 조회
            axios.get("http://localhost:3000/cartoon_info/" + modifyedPk)
                 .then((response) => { return response.data; })
                 .then((response) => { viewSelectInfoData(response, modifyedPk, category) })
                 .catch((error) => console.log(error))
        } else if(category == "notice") { // 공지사항 정보 조회
            axios.get("http://localhost:3000/notice_info/" + modifyedPk)
                 .then((response) => { return response.data; })
                 .then((response) => { viewSelectInfoData(response, modifyedPk, category) })
                 .catch((error) => console.log(error))
        }
    })

}

// 회원정보 수정(서버 데이터, 서버 객체 id값, 카테고리(회원, 기업, 웹툰))
function viewSelectInfoData(infoList, pk, category) {

    let inputData = user_info.querySelectorAll("input");
    let keyGroup = []; // 사용자 입력 회원정보 항목

     // 사용자 회원정보 항목 추출
    for(let i = 0; i < inputData.length; i++) {
        let names = inputData[i].getAttribute("name");
        keyGroup.push(names);
    }

    if(category == "user") { // 회원리스트

        // 회원정보 조회(모달창)
        for(let i = 0; i < keyGroup.length; i++) {
            if(keyGroup[i] == "pk" || keyGroup[i] == "type" || keyGroup[i] == "latestLogin") user_info[keyGroup[i]].value = infoList[keyGroup[i]];
            else if(keyGroup[i] == "companyName") user_info.companyName.value = infoList.company.name;
            else user_info[keyGroup[i]].value = infoList.personal[keyGroup[i]];
        }

        // 회원정보 변경 및 서버 전송
        let submitBtn = document.querySelector(".submit_btn");
        submitBtn.addEventListener("click", (e) => {
            e.preventDefault();
            console.log("clicked");
            for(let i = 0; i < keyGroup.length; i++) {
                if(keyGroup[i] == "pk" || keyGroup[i] == "type" || keyGroup[i] == "latestLogin") infoList[keyGroup[i]] = user_info[keyGroup[i]].value;
                else if(keyGroup[i] == "companyName") infoList.company.name = user_info.companyName.value;
                else infoList.personal[keyGroup[i]] = user_info[keyGroup[i]].value;
            }
            axios.put("http://localhost:3000/user_info/" + pk, infoList)
                .then((response) => { console.log(response); })
                .catch((error) => console.log(error))
        })
    } else if(category == "company") { // 기업리스트

        // 기업정보 조회(모달창)
        for(let i = 0; i < keyGroup.length; i++) {
            if(keyGroup[i] == "type") user_info[keyGroup[i]].value = infoList[keyGroup[i]];
            else user_info[keyGroup[i]].value = infoList.company[keyGroup[i]];
        }

        // 기업정보 변경 및 서버 전송
        let submitBtn = document.querySelector(".submit_btn");
        submitBtn.addEventListener("click", (e) => {
            e.preventDefault();
            console.log("clicked");
            for(let i = 0; i < keyGroup.length; i++) {
                if(keyGroup[i] == "type") infoList[keyGroup[i]] = user_info[keyGroup[i]].value;
                else infoList.company[keyGroup[i]] = user_info[keyGroup[i]].value;
            }
            axios.put("http://localhost:3000/user_info/" + pk, infoList)
                .then((response) => { console.log(response); })
                .catch((error) => console.log(error))
        })
    } else if(category == "webtoon") { // 웹툰리스트

        if(infoList["page"] == "webtoons") { // 한국기업

            // 모달창 기업정보 조회
            for(let i = 0; i < keyGroup.length; i++) {
                if(keyGroup[i] == "name") user_info[keyGroup[i]].value = infoList.company[keyGroup[i]];
                else user_info[keyGroup[i]].value = infoList[keyGroup[i]];
            }

            // 수정한 정보 서버로 전달
            let submitBtn = document.querySelector(".submit_btn");
            submitBtn.addEventListener("click", (e) => {
                e.preventDefault();
                console.log("clicked");
                for(let i = 0; i < keyGroup.length; i++) {
                    if(keyGroup[i] == "name") infoList.company[keyGroup[i]] = user_info[keyGroup[i]].value;
                    else infoList[keyGroup[i]] = user_info[keyGroup[i]].value;
                }
                axios.put("http://localhost:3000/cartoon_info/" + pk, infoList)
                    .then((response) => { console.log(response); })
                    .catch((error) => console.log(error))
            })

        } else if(infoList["page"] == "creators") { // 자유투고

            // 모달창 기업정보 조회
            for(let i = 0; i < keyGroup.length; i++) {
                if(keyGroup[i] == "name") user_info[keyGroup[i]].value = infoList.author[keyGroup[i]];
                else if(keyGroup[i] == "vote" || keyGroup[i] == "view") continue;
                else user_info[keyGroup[i]].value = infoList[keyGroup[i]];
            }

            // 수정한 정보 서버로 전달
            let submitBtn = document.querySelector(".submit_btn");
            submitBtn.addEventListener("click", (e) => {
                e.preventDefault();
                console.log("clicked");
                for(let i = 0; i < keyGroup.length; i++) {
                    if(keyGroup[i] == "name") infoList.author[keyGroup[i]] = user_info[keyGroup[i]].value;
                    else if(keyGroup[i] == "vote" || keyGroup[i] == "view") continue;
                    else infoList[keyGroup[i]] = user_info[keyGroup[i]].value;
                }
                axios.put("http://localhost:3000/cartoon_info/" + pk, infoList)
                    .then((response) => { console.log(response); })
                    .catch((error) => console.log(error))
            })

        }
    } else if(category == "notice") { // 공지사항 리스트

        // 모달창 공지사항 정보 조회
        for(let i = 0; i < keyGroup.length; i++) {
            user_info[keyGroup[i]].value = infoList[keyGroup[i]];
        }

        // 공지사항 정보 변경 및 서버 전송
        let submitBtn = document.querySelector(".submit_btn");
        submitBtn.addEventListener("click", (e) => {
            e.preventDefault();
            console.log("clicked");
            for(let i = 0; i < keyGroup.length; i++) {
                infoList[keyGroup[i]] = user_info[keyGroup[i]].value;
            }
            axios.put("http://localhost:3000/notice_info/" + pk, infoList)
                .then((response) => { console.log(response); })
                .catch((error) => console.log(error))
        })
        
    }
}