import modifyContents from './modify.js'
import deleteContents from './delete.js';

// 데이터 조회(카테고리 : 회원, 기업)
export default function viewContentsData(category) {

    if(category == "user" || category == "company" || category == "matching") { // 회원, 기업, 매칭 정보
        axios.get("http://localhost:3000/user_info")
        .then((response) => { return response.data; })
        .then((response) => { createContents(response, category) })
        .catch((error) => console.log(error))
    } else if(category == "webtoon") { // 웹툰 정보
        axios.get("http://localhost:3000/cartoon_info")
        .then((response) => { return response.data; })
        .then((response) => { createContents(response, category) })
        .catch((error) => console.log(error))
    } else if(category == "notice") { // 공지사항 정보
        axios.get("http://localhost:3000/notice_info")
        .then((response) => { return response.data; })
        .then((response) => { createContents(response, category) })
        .catch((error) => console.log(error))
    }
    
}

// 요소 생성 함수(조회 데이터, 데이터 타입(회원, 기업, 웹툰))
function createContents(InfoList, category) {

    let table = document.querySelector("tbody"); // 요소 생성할 리스트
    if(category == "user") { // 회원리스트
        let userItem; 
        InfoList.sort((a,b) => a.pk - b.pk);
        for(let info of InfoList) {
            if(info.type == "normal") { // 노말유저인 경우 (기업정보 없음)
                userItem = `
                    <tr data-pk=${info.id}>
                        <td>${info.pk}</td>
                        <td>${info.type}</td>
                        <td>${info.personal.id}</td>
                        <td>${info.personal.name}</td>
                        <td>${info.personal.officePhoneNumber}</td>
                        <td>${info.personal.phoneNumber}</td>
                        <td>${info.personal.department}</td>
                        <td>${info.personal.email}</td>
                        <td></td>
                        <td>${info.latestLogin}</td>
                    </tr>
                `;
            } else { // 한국기업, 해외바이어인 경우 (기업정보 있음)
                console.log(info);
                userItem = `
                    <tr data-pk=${info.id}>
                        <td>${info.pk}</td>
                        <td>${info.type}</td>
                        <td>${info.personal.id}</td>
                        <td>${info.personal.name}</td>
                        <td>${info.personal.officePhoneNumber}</td>
                        <td>${info.personal.phoneNumber}</td>
                        <td>${info.personal.department}</td>
                        <td>${info.personal.email}</td>
                        <td>${info.company.name}</td>
                        <td>${info.latestLogin}</td>
                    </tr>
                `;
            }
            table.innerHTML += userItem;
        }

        // 회원리스트 안의 모든 요소
        let listElement = document.querySelectorAll("tbody tr");
        modifyContents(listElement); // 선택한 요소 수정
        deleteContents(listElement, category); // 선택한 요소 삭제
        

    } else if(category == "company" || category == "webtoon") { // 기업 및 웹툰 리스트

        let typeBtn = document.querySelectorAll(".type_btn"); // 회사 타입 선택 탭메뉴( Buyer, Seller )
        if(category == "company") createCompanyElement(InfoList, "buyer", table, category); // 버튼 클릭전 첫화면에서 Buyer 회사 조회
        else if(category == "webtoon") createCompanyElement(InfoList, "webtoons", table, category); // 버튼 클릭전 첫화면에서 company 웹툰 조회
        for(let btn of typeBtn) { 
            btn.addEventListener("click", function() {
                for(let btn of typeBtn) btn.classList.remove("selected");
                this.classList.add("selected"); // 탭선택 효과 적용
                let companyType = btn.getAttribute("category"); // 선택한 탭의 회사 타입
                createCompanyElement(InfoList, companyType, table, category); // 선택한 타입의 회사 조회 
            })
        }

    } else if(category == "matching") { // 매칭리스트

        let matchingData = []; // 매칭 데이터만 담는 배열 
        for(let info of InfoList) { // 매칭 데이터 추출
            let matchingInfo = info.matching;
            if(matchingInfo) { // 매칭 정보 존재할 경우
                for(let i = 0; i < matchingInfo.length; i++) {
                    matchingData.push(matchingInfo[i]);
                }
            }
        }

        matchingData.sort((a,b) => a.matching_pk - b.matching_pk); // 매칭번호순 정렬
        let matched_count = 0; // 매칭 성사 횟수
        let cancel_count = 0; // 매칭 실패 횟수
        let proceed_count = 0; // 매칭 대기 횟수

        for(let info of matchingData) { // 매칭 요소 생성
            let userItem; // 유저 아이템
            userItem = `
                <tr>
                    <td>${info.matching_pk}</td>
                    <td>${info.request_date}</td>
                    <td>${info.company_name}</td>
                    <td>${info.name}</td>
                    <td>${info.status}</td>
                </tr>
            `;
            table.innerHTML += userItem;

            if(info.status == "Matched") matched_count++; // 매칭성공일 경우 매칭성공 횟수 + 1
            else if(info.status == "Cancel") cancel_count++; // 매칭실패일 경우 매칭실패 횟수 + 1
            else if(info.status == "Proceed") proceed_count++; // 매칭대기일 경우 매칭대기 횟수 + 1
        }

        // 매칭 성공/실패/대기 숫자 집계
        let matchingWrapper = document.querySelector(".matching_wrapper");
        let matchingTotal = `
            <div class="matching_total">
                <div class="matched">매칭성공 : <span class="number">${matched_count}</span></div>
                <div class="cancel">매칭실패 : <span class="number">${cancel_count}</span></div>
                <div class="proceed">매칭대기 : <span class="number">${proceed_count}</span></div>
            </div>
        `;
        matchingWrapper.insertAdjacentHTML("beforeend", matchingTotal);

    } else if(category == "notice") { // 공지사항 리스트

        InfoList.sort((a,b) => a.pk - b.pk); // 공지사항 번호순 정렬
        for(let info of InfoList) {
            let userItem; 
            userItem = `
                <tr data-pk="${info.id}">
                    <td>${info.pk}</td>
                    <td>${info.title}</td>
                    <td>${info.writer}</td>
                    <td>${info.update}</td>
                </tr>
            `;
            table.innerHTML += userItem;
        }

        // 공지사항리스트 안의 모든 요소
        let listElement = document.querySelectorAll("tbody tr");
        modifyContents(listElement); // 선택한 요소 수정
        deleteContents(listElement, category); // 선택한 요소 삭제
    }
}

// 조회한 회사 요소 생성(전체 회사 데이터, 회사 유형, 리스트 요소)
function createCompanyElement(info, type, table, category) {

    table.innerHTML = ``; // 요소 생성할 리스트 비우기
    let companyTypeData = []; // 회사유형별 데이터 (Buyer, Seller)

    for(let i = 0; i < info.length; i++) {
        if(category == "company") {
            if(info[i].type == type) companyTypeData.push(info[i]); // 기업타입 일치하는 데이터만 추출
        } else if(category == "webtoon") {
            if(info[i].page == type) companyTypeData.push(info[i]); // 웹툰타입 일치하는 데이터만 추출
        }
    }

    if(category == "company") companyTypeData.sort((a,b) => a.company.pk - b.company.pk); // 기업 고유번호순 정렬
    else if(category == "webtoon") companyTypeData.sort((a,b) => a.pk - b.pk); // 웹툰 고유번호순 정렬

    if(category == "company") { // 선택한 타입(Buyer, Seller)의 기업 데이터로만 요소 생성
        for(let i = 0; i < companyTypeData.length; i++) {
            let userItem = `
                <tr data-pk=${companyTypeData[i].id}>
                    <td>${companyTypeData[i].company.pk}</td>
                    <td>${companyTypeData[i].type}</td>
                    <td>${companyTypeData[i].company.name}</td>
                    <td>${companyTypeData[i].company.ceo}</td>
                    <td>${companyTypeData[i].company.website}</td>
                    <td>${companyTypeData[i].company.address}</td>
                    <td>${companyTypeData[i].company.fax}</td>
                </tr>
            `;
            table.innerHTML += userItem;
        }
    } else if(category == "webtoon") {  // 선택한 타입(webtoons, creators)의 웹툰 데이터로만 요소 생성
        let userItem;
        for(let i = 0; i < companyTypeData.length; i++) {
            if(type == "webtoons") { // 한국기업 웹툰
                userItem = `
                    <tr data-pk=${companyTypeData[i].id}>
                        <td>${companyTypeData[i].pk}</td>
                        <td>${companyTypeData[i].page}</td>
                        <td>${companyTypeData[i].title}</td>
                        <td>${companyTypeData[i].company.name}</td>
                        <td>${companyTypeData[i].genre.replace("All, ", "")}</td>
                        <td>${companyTypeData[i].uploadDate}</td>
                        <td>${companyTypeData[i].vote}</td>
                        <td>${companyTypeData[i].view}</td>
                    </tr>
                `;
            } else if(type == "creators") { // 자유투고 웹툰
                userItem = `
                    <tr data-pk=${companyTypeData[i].id}>
                        <td>${companyTypeData[i].pk}</td>
                        <td>${companyTypeData[i].page}</td>
                        <td>${companyTypeData[i].title}</td>
                        <td>${companyTypeData[i].author.name}</td>
                        <td>${companyTypeData[i].genre.replace("All, ", "")}</td>
                        <td>${companyTypeData[i].uploadDate}</td>
                        <td></td>
                        <td></td>
                    </tr>
                `;
            }
            table.innerHTML += userItem;
        }
    }
    

    // 기업, 웹툰 리스트 내의 모든 요소
    let listElement = document.querySelectorAll("tbody tr");
    modifyContents(listElement); // 선택한 요소 수정
    deleteContents(listElement, category); // 선택한 요소 삭제

}


