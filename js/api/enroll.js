export default function enrollContents() {
     // 등록 모달창 입력값 모두 비우기
     let inputData = user_info.querySelectorAll("input");
     for(let i = 0; i < inputData.length; i++)  inputData[i].value = '';

     // 등록버튼 클릭
     let submitBtn = document.querySelector(".submit_btn");
     submitBtn.addEventListener("click", (e) => {
         let category = e.target.dataset.type;
         e.preventDefault();
         enrollNewUserInfo(category);
     });
}

// 새로운 회원 등록
function enrollNewUserInfo(category) {

    let inputData = user_info.querySelectorAll("input");
    let keyGroup = []; // 사용자 입력 회원정보 항목

    // 사용자 입력 회원정보 항목 추출
    for(let i = 0; i < inputData.length; i++) {
        let names = inputData[i].getAttribute("name");
        keyGroup.push(names);
    }

    let enrolledData;  // 등록할 데이터 정보
    
    if(category == "user") { // 회원리스트
        enrolledData = { personal : {}, company : {} };
        for(let i = 0; i < keyGroup.length; i++) {
            if(keyGroup[i] == "pk" || keyGroup[i] == "type" || keyGroup[i] == "latestLogin") enrolledData[keyGroup[i]] = user_info[keyGroup[i]].value;
            else if(keyGroup[i] == "companyName") enrolledData.company.name = user_info[keyGroup[i]].value;
            else enrolledData.personal[keyGroup[i]] = user_info[keyGroup[i]].value;
        }
    } else if(category == "company") { // 기업리스트
        enrolledData = { company : {} };
        for(let i = 0; i < keyGroup.length; i++) {
            if(keyGroup[i] == "type") enrolledData[keyGroup[i]] = user_info[keyGroup[i]].value;
            else enrolledData.company[keyGroup[i]] = user_info[keyGroup[i]].value;
        }
    } else if(category == "webtoon") { // 웹툰리스트
        if(user_info["page"].value == "webtoons") { // 웹툰유형 webtoons(한국기업)
            enrolledData = { company : {} }; 
            for(let i = 0; i < keyGroup.length; i++) {
                if(keyGroup[i] == "name") enrolledData.company[keyGroup[i]] = user_info[keyGroup[i]].value;
                else enrolledData[keyGroup[i]] = user_info[keyGroup[i]].value;            
            }
        } else if(user_info["page"].value == "creators") { // 웹툰유형 creators(자유투고)
            enrolledData = { author : {} }; 
            for(let i = 0; i < keyGroup.length; i++) {
                if(keyGroup[i] == "name") enrolledData.author[keyGroup[i]] = user_info[keyGroup[i]].value;
                else if(keyGroup[i] == "vote" || keyGroup[i] == "view") continue;
                else enrolledData[keyGroup[i]] = user_info[keyGroup[i]].value;            
            }
        }
    } else if(category == "notice") { // 공지사항 리스트
        enrolledData = {};
        for(let i = 0; i < keyGroup.length; i++) {
            enrolledData[keyGroup[i]] = user_info[keyGroup[i]].value;
        }
    }
    
    // 서버에 데이터 전송 
    if(category == "user" || category == "company") {
        axios.post("http://localhost:3000/user_info", enrolledData)
             .then((response) => { console.log(response); })
             .catch((error) => { console.log(error); })
    } else if(category == "webtoon") {
        axios.post("http://localhost:3000/cartoon_info", enrolledData)
             .then((response) => { console.log(response); })
             .catch((error) => { console.log(error); })
    } else if(category == "notice") {
        axios.post("http://localhost:3000/notice_info", enrolledData)
             .then((response) => { console.log(response); })
             .catch((error) => { console.log(error); })
    }
    
}