// 메뉴 생성 함수
export default function createLeftMenu() {

    let main = document.getElementById("main");
    let leftmenu = `
        <div id="left_menu">
            <a href="manage_user.html" class="menu">회원관리</a>
            <a href="manage_company.html" class="menu">기업관리</a>
            <a href="manage_contents.html" class="menu">콘텐츠관리</a>
            <a href="manage_matching.html" class="menu">매칭관리</a>
            <a href="manage_notice.html" class="menu">공지사항</a>
            <a href="manage_statistic.html" class="menu">통계</a>
        </div>
    `;
    main.insertAdjacentHTML("afterbegin", leftmenu);
    
}
