import enrollContents from './enroll.js';

// 모달창 열기
export default function openModal() {
    let modalWrapper = document.querySelector(".modal-wrapper");
    let enrollBtn = document.getElementById("enroll");

    enrollBtn && enrollBtn.addEventListener("click", () => {
        modalWrapper.classList.remove("off");
        enrollContents(); // 요소 등록
    })

    closeModal();
}

// 모달창 닫기
function closeModal() {
    let modalWrapper = document.querySelector(".modal-wrapper");
    let modal = document.querySelector(".modal");
    let closeBtn = document.querySelector(".close_btn");

    modalWrapper.addEventListener("click", function() {
        this.classList.add("off");
    })
    modal.addEventListener("click", function(e) {
        e.stopPropagation();
    })
    closeBtn.addEventListener("click", () => {
        modalWrapper.classList.add("off");
    })
}




