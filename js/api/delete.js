// 요소 삭제 함수
export default function deleteContents(listElement, category) {
  for(let ele of listElement) { 
      ele.addEventListener("click", function() {
          for(let ele of listElement) ele.classList.remove("clicked");
          this.classList.add("clicked"); // 요소 선택 효과 적용
          let deletedPk = this.dataset.pk; // 삭제할 요소 pk값
          const deleteBtn = document.getElementById("delete"); // 삭제 버튼
          deleteBtn.addEventListener("click", () => { // 요소 선택 후 삭제 버튼 클릭
              console.log(deletedPk);
              if(category == "user" || category == "company") { // 회원리스트, 기업리스트
                axios.delete("http://localhost:3000/user_info/" + deletedPk)
                .then((response) => { console.log(response); }) 
                .catch((error) => console.log(error))
              } else if(category == "webtoon") { // 웹툰리스트
                axios.delete("http://localhost:3000/cartoon_info/" + deletedPk)
                .then((response) => { console.log(response); }) 
                .catch((error) => console.log(error))
              } else if(category == "notice") { // 공지사항 리스트
                axios.delete("http://localhost:3000/notice_info/" + deletedPk)
                .then((response) => { console.log(response); }) 
                .catch((error) => console.log(error))
              }
          })
      })
  }
}