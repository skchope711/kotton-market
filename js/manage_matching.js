import createHeader from './api/header.js';
import createLeftMenu from './api/leftmenu.js';
import createContents from './api/contents.js';
import openModal from './api/modal.js';

createHeader(); // 헤더 생성
createLeftMenu(); // 왼쪽 메뉴 생성
createContents("matching"); // 매칭정보 조회 및 리스트 생성
openModal(); // 모달창 열기 